export default function FakeAPI(token) {
  console.log("check token in fakeAPI: ", { token });
  return new Promise((res, rej) =>
    setTimeout(() => {
      if (token !== "123456789") {
        return rej(Error("Invalid token"));
      }
      res({ name: "test", age: 18 });
    }, 2000)
  );
}
