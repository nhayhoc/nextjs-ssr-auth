import React, { useState } from "react";
import { setCookie } from "../lib/cookie";
import { useRouter } from "next/router";

export default function login() {
  const router = useRouter();
  const [password, setPassword] = useState("");
  return (
    <div>
      <input value={password} onChange={(e) => setPassword(e.target.value)} />
      <br />
      <button
        onClick={() => {
          //set cookie
          setCookie("token", password, 1);
          router.push("/profile/csr");
        }}
      >
        Login && redirect to CSR
      </button>
      <br />
      <button
        onClick={() => {
          //set cookie
          setCookie("token", password, 1);
          router.push("/profile/ssr");
        }}
      >
        Login && redirect to SSR
      </button>
    </div>
  );
}
