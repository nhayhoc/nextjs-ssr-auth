import React, { useEffect, useState } from "react";
import FakeAPI from "../../lib/fakeAPI";
import { getCookie } from "../../lib/cookie";
// hiện page ở dạng static page đã được render khi build, khi vào page này thì nó sẽ chạy useeffect và gọi api login
// nếu muốn nó render ở client thì phải dùng dynamic import với option ssr false
export default function csr() {
  const [isLogin, setIsLogin] = useState(false);
  const [user, setUser] = useState({ name: "", age: 0 });
  useEffect(() => {
    let token = getCookie("token");
    FakeAPI(token)
      .then((res) => {
        console.log({ res });
        setUser(res);
        setIsLogin(true);
      })
      .catch((err) => {
        //
      });
  }, []);

  return (
    <div>
      <h1>{!isLogin ? "NOT LOGIN" : "LOGGED"}</h1>
      <pre>{JSON.stringify(user)}</pre>
    </div>
  );
}
