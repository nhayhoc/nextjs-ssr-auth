import React from "react";
import FakeAPI from "../../lib/fakeAPI";

export default function ssr({ isLogin, user }) {
  return (
    <div>
      <h1>{!isLogin ? "NOT LOGIN" : "LOGGED"}</h1>
      <pre>{JSON.stringify(user)}</pre>
    </div>
  );
}
export async function getServerSideProps({ req, res }) {
  try {
    const { cookie } = req.headers;
    //   code demo
    const token = cookie.split("=")[1];

    const user = await FakeAPI(token);
    return {
      props: {
        user,
        isLogin: true,
      },
    };
  } catch (error) {
    return {
      props: {
        // if not login, return ??
      },
    };
  }
}
